;Name : Jun Han Ooi 
;CISC 3160 
;Lab 7

(defun mylength (lst)
  (if (null lst)
    0
    (+ 1 (mylength (cdr lst)))))

(defun mymerge (lst1 lst2)
  (cond 
    ((null lst1) lst2)
    ((null lst2) lst1)
    ((<= (car lst1) (car lst2))
        (cons (car lst1) (mymerge (cdr lst1) lst2)))
    (T 
      (cons (car lst2) (mymerge lst1 (cdr lst2))))))

(defun allbutlast (lst)
  (cond
    ((null lst) nil)
    ((= (length lst) 1) nil)
    (T
      (cons (car lst) (allbutlast (cdr lst))))))

(defun mylast (lst)
  (cond
    ((null lst) nil)
    ((= (length lst) 1) lst)
    (T
      (mylast (cdr lst)))))

(defun rcons (val lst)
  (append lst (list val)))

(defun myreverse (lst)
  (if (null lst)
    nil
    (append (myreverse (cdr lst)) (list (car lst)))))

(defun palindromep (lst)
  (cond 
    ((null lst) T)
    ( (equal (car lst) (car (last lst)))
     (palindromep (cdr (allbutlast lst))))))

(defun mysearch (lst val)
  (cond
    ( (null lst) nil )
    ( (equal (car lst) val) T)
    (T
      ( mysearch  (cdr lst) val ) ) ) )  

(defun find-and-do (lst val fun)
  (cond 
    ( (null lst) nil )
    ( (equal (car lst) val ) (cons (funcall fun val) (find-and-do (cdr lst) val fun ) ) ) 
    ( T 
      (cons (car lst)  (find-and-do (cdr lst) val fun ) ) ) ) )

(defun test-and-do (lst predp fun) 
  (cond 
    ( (null lst) nil )
    ( (funcall predp (car lst) ) (cons (funcall fun (car lst) ) (test-and-do (cdr lst) predp fun ) ) ) 
    ( T 
      (cons (car lst) (test-and-do (cdr lst) predp fun) ) ) ) )

(print "mylength")
(print (mylength '(a b c d)))
(print (mylength '(a)))
(print (mylength '()))

(print "mymerge")
(print (mymerge '(1 2 3) '(2 3 4)))
(print (mymerge '(3 4 5) '(3 3 3)))
(print (mymerge '(1 2 3) '(1 1 1)))

(print "allbutlast")
(print (allbutlast '(1 2 3 4)))
(print (allbutlast '()))
(print (allbutlast '(2)))

(print "mylast")
(print (mylast '(a b c)))
(print (mylast '(a b)))
(print (mylast '(a)))
(print (mylast '()))

(print "myReverse")
(print (myreverse '(a b c)))
(print (myreverse '(a b c d)))
(print (myreverse '(a b)))
(print (myreverse '()))

(print "rcons")
(print (rcons 'A '()))
(print (rcons 'C '(A B)))
(print (rcons 'D '(A B C)))
(print (rcons 'E '(Q W R)))

(print "palindromep")
(print (palindromep '(a b a)))
(print (palindromep '()))
(print (palindromep '(A B C)))
(print (palindromep '(a a b b)))
(print (palindromep '(a a e e)))
(print (palindromep '(a a)))

(print "mysearch")
(print (mysearch '() 'a))
(print (mysearch '(a) 'a))
(print (mysearch '(a b c) 'a))
(print (mysearch '(a b c) 'd))
(print (mysearch '(b b b b) 'a))

(print "find-and-do")
(print (find-and-do '(1 2 3) 2 #'sqrt) )
(print (find-and-do '() 1 (lambda (x) (+ x 1))) )
(print (find-and-do '(1 2 3) 2 (lambda (x) (* x 2))) )
(print (find-and-do '(a b c) 'b #'list) )
(print (find-and-do '(1 2 3 4) 9 (lambda (x) (* x x))))
(print (find-and-do '(1 2 3 4) 4 (lambda (x) (* x x))))

(print "test-and-do")
(print (test-and-do '() #'evenp (lambda (x) (+ x 1))) )
(print (test-and-do '(1 2 3 4) #'evenp (lambda (x) (* x 2))) )
(print (test-and-do '() #'oddp (lambda (x) (+ x 1))) )
(print (test-and-do '(1 2 3 4) #'oddp (lambda (x) (* x 2))) )
(print (test-and-do '(0 2 2 2) #'zerop (lambda (x) (+ x 1))))
(print (test-and-do '(-1 2 2 2) #'minusp (lambda (x) (* x 100))))

(exit)
