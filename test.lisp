

(defun tripe (x) (* x 3))

(defun mylengthlazy (lst)
  (length lst))

(defun mylength (lst)
  (if (null lst)
    0
    (+ 1 (mylength (cdr lst)))))

(defun mymerge (lst1 lst2)
  (sort
    (append lst1 lst2) #'< ))

(defun myreverse (lst1 lst2)
  (sort 
    (append lst1 lst2) #'> ))

(defun allButLast (lst)
  (butlast lst))

(defun palindrome (lst) 
  (equal lst (reverse lst)))

(defun mysearch (lst val)
  (if (null (member val lst)) T
  nil)  

(print (tripe 4))
(print (mylength '(1 2 3 5 6) ))
(print (sort (append '(9 1 3 4) '(5 6 7 8)) #'> ) )
(print (mymerge '(9 9 1) '(1 2 3)))
(print (myreverse '(9 9 1) '(1 2 3)))
(print (allbutLast '(1 2 3 4 5 6 7 111)))
(print (palindrome '(b o b)))
(print (palindrome '(b a o)))
(print (palindrome '()))

(exit)
